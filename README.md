#BashImGet
Easy reader [bash.im](http://bash.im) written in Java

##Build
Netbeans...

Or else (for unix users):  
``` bash
$ make
```

Or else (for arch users):
``` bash
$ yaourt -S bashimget
```
``` bash
$ pacaur -S bashimget
```
[AUR](https://aur4.archlinux.org/packages/bashimget)

##Example
``` bash
$ java -jar BashImGet.jar /quote/433901
2015-05-25 12:44 | #433901
XXX: Я правильно понял, что конструкция
function somefun() : void
означает, что мы пишем функцию, не возвращающую значений?
YYY: да, правильно
YYY: а это что за язык вообще?
```
``` bash
$ java -jar BashImGet.jar / > bash.im.txt
```
