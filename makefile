MANIFEST=./manifest.mf

BUILD=./build
DIST=./dist
SRC=./src
LIB=$(DIST)/lib

OUTPUT=BashImGet.jar
MAIN_CLASS=com/kalterfive/bashimget/Main.java

JC=javac
CP=-classpath $(LIB)/jsoup-1.8.2.jar
JCOPTS=-sourcepath $(SRC) -d $(BUILD) -g $(SRC)/$(MAIN_CLASS)
PACKCMD=jar cfm $(DIST)/$(OUTPUT) $(MANIFEST) -C $(BUILD) .

all: init clean compile pack

init:
	mkdir -p $(BUILD)
	mkdir -p $(DIST)

clean:
	rm -rf $(BUILD)/*
	rm -rf $(DIST)/$(OUTPUT)

compile:
	$(JC) $(CP) $(JCOPTS)

pack:
	for j in $(LIB)/*.jar ; do \
		unzip $$j -x META-INF/* -d $(BUILD) > /dev/null ; \
	done
	$(PACKCMD)

install:
	mkdir -p $(DESTDIR)
	mkdir -p $(PREFIX)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 $(DIST)/$(OUTPUT) $(DESTDIR)$(PREFIX)/bin/$(OUTPUT)
