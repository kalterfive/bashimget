package com.kalterfive.bashimget;

/**
 * @author kalterfive
 */
public class Quote {
	
	private final String date;
	private final String number;
	private final String quote;
	
	public Quote(String date, String number, String quote) {
		this.date = date;
		this.number = number;
		this.quote = quote;
	}
	
	@Override
	public String toString() {
		return String.format("%s | %s\n%s\n", date, number, quote);
	}
}
