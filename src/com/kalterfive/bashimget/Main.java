package com.kalterfive.bashimget;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

/**
 * @author kalterfive
 */
public class Main {

	public static void main(String[] args) {
		if (args.length < 1) {
			printHelp();
			return;
		}

		try {
			final String url = "http://www.bash.im" + args[0];
			new Main().parse(url).print();
		} catch (IOException e) {
		}
	}

	private static void printHelp() {
		// TODO
	}

	private final List<Quote> quotes;

	private Main() {
		quotes = new ArrayList<>();
	}

	private Main parse(String url) throws IOException {
		final Document bash = Jsoup.connect(url).get();
		final List<Element> elms = bash.select(".quote");
		elms.stream().forEach((elem) -> {
			final String date = get(elem, "span[class=date]");
			final String number = get(elem, "a[class=id]");
			final String text = get(elem, "div[class=text]");

			if (text != null) {
				quotes.add(new Quote(date, number, text));
			}
		});

		return this;
	}

	private String get(Element element, String cssQuery) {
		final Element result = element.select(cssQuery).first();
		if (result != null) {
			return result.html()
					.replaceAll("(<br>)\n+(\\1)*", "\n")
					.replaceAll("<br>", "")
					.replaceAll("&lt;", "<")
					.replaceAll("&gt;", ">");
		}

		return null;
	}

	private void print() {
		quotes.stream().forEach(System.out::println);
	}
}
